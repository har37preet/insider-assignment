package com.app.insider.data.remote;

import com.app.insider.BuildConfig;
import com.app.insider.data.model.OpenStreetMap.Response;
import com.app.insider.data.model.home.HomeResponseModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface IRemoteDataSource {
    String BASE_URL = "https://api.insider.in/";
    int TIMEOUT = 10;
    int READ_TIMEOUT = 10;

    @GET("home")
    Observable<HomeResponseModel> getHomeData(@Query("norm") int norm, @Query("filterBy") String filterBy, @Query("city") String city);

    @GET
    Observable<HomeResponseModel> getResponse(@Url String url);

    @GET
    Observable<Response> getCity(@Url String url);
}