package com.app.insider.data.model.home;

import com.google.gson.annotations.SerializedName;

public class DigitalEventGroupModel {
    @SerializedName("tag_id")
    String tagId;

    String title;
    String url;
    @SerializedName("thumbnail_url")
    String thumbnailUrl;

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
