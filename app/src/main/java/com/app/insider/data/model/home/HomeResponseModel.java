package com.app.insider.data.model.home;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Map;

public class HomeResponseModel {
    ArrayList<String> tags;
    ArrayList<String> groups;
    Map<String, ShowModelObject> filters;
    Map<String, ArrayList<ShowModel>> sorters;
    ListModel list;
    ListModel picks;
    ArrayList<EventModel> popular;
    ArrayList<EventModel> featured;
    Map<String,String> dates;
    ArrayList<BannerModel> banners;
    @SerializedName("digital_event_groups")
    ArrayList<DigitalEventGroupModel> digitalEventGroupModels;
    @SerializedName("digital_event_groups_description")
    String digitalEventGroupsDescription;

    public Map<String, ShowModelObject> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, ShowModelObject> filters) {
        this.filters = filters;
    }

    public Map<String, ArrayList<ShowModel>> getSorters() {
        return sorters;
    }

    public void setSorters(Map<String, ArrayList<ShowModel>> sorters) {
        this.sorters = sorters;
    }

    public ArrayList<EventModel> getPopular() {
        return popular;
    }

    public void setPopular(ArrayList<EventModel> popular) {
        this.popular = popular;
    }

    public ArrayList<EventModel> getFeatured() {
        return featured;
    }

    public void setFeatured(ArrayList<EventModel> featured) {
        this.featured = featured;
    }

    public Map<String, String> getDates() {
        return dates;
    }

    public void setDates(Map<String, String> dates) {
        this.dates = dates;
    }

    public String getDigitalEventGroupsDescription() {
        return digitalEventGroupsDescription;
    }

    public void setDigitalEventGroupsDescription(String digitalEventGroupsDescription) {
        this.digitalEventGroupsDescription = digitalEventGroupsDescription;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public ArrayList<String> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<String> groups) {
        this.groups = groups;
    }

    public ArrayList<BannerModel> getBanners() {
        return banners;
    }

    public void setBanners(ArrayList<BannerModel> banners) {
        this.banners = banners;
    }

    public ArrayList<DigitalEventGroupModel> getDigitalEventGroupModels() {
        return digitalEventGroupModels;
    }

    public void setDigitalEventGroupModels(ArrayList<DigitalEventGroupModel> digitalEventGroupModels) {
        this.digitalEventGroupModels = digitalEventGroupModels;
    }

    public ListModel getList() {
        return list;
    }

    public void setList(ListModel list) {
        this.list = list;
    }

    public ListModel getPicks() {
        return picks;
    }

    public void setPicks(ListModel picks) {
        this.picks = picks;
    }
}
