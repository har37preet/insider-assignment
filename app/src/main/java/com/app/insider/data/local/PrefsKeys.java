package com.app.insider.data.local;

public class PrefsKeys {

    public static final String ACCESSTOKEN = "accessToken";

    public static final String LOGINSTATUS = "loginStatus";
}
