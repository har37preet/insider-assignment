package com.app.insider.data.constants;

public class Constants {
    public static final int SUCCESS_CODE = 200;
    public static final int UNAUTHORIZED = 401;
    public static final String USER_NOT_LOGGED_IN = "USER_NOT_LOGGED_IN";
}