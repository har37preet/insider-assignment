package com.app.insider.data.model.home;

import com.google.gson.annotations.SerializedName;

public class FavStats {
    @SerializedName("target_id")
    String targetId;
    int actualCount;
    int prettyCount;

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public int getActualCount() {
        return actualCount;
    }

    public void setActualCount(int actualCount) {
        this.actualCount = actualCount;
    }

    public int getPrettyCount() {
        return prettyCount;
    }

    public void setPrettyCount(int prettyCount) {
        this.prettyCount = prettyCount;
    }
}
