package com.app.insider.data.model.home;

import com.google.gson.annotations.SerializedName;

public class BannerModel {
    @SerializedName("_id")
    String id;

    @SerializedName("is_internal")
    boolean isInternal;

    String name;
    String type;

    @SerializedName("category_id")
    CategoryModel categoryModel;

    @SerializedName("group_id")
    GroupModel groupModel;
    @SerializedName("map_link")
    String mapLink;

    @SerializedName("vertical_cover_image")
    String verticalCoverImage;

    int priority;

    @SerializedName("display_details")
    DisplayDetail displayDetail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isInternal() {
        return isInternal;
    }

    public void setInternal(boolean internal) {
        isInternal = internal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public GroupModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(GroupModel groupModel) {
        this.groupModel = groupModel;
    }

    public String getMapLink() {
        return mapLink;
    }

    public void setMapLink(String mapLink) {
        this.mapLink = mapLink;
    }

    public String getVerticalCoverImage() {
        return verticalCoverImage;
    }

    public void setVerticalCoverImage(String verticalCoverImage) {
        this.verticalCoverImage = verticalCoverImage;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public DisplayDetail getDisplayDetail() {
        return displayDetail;
    }

    public void setDisplayDetail(DisplayDetail displayDetail) {
        this.displayDetail = displayDetail;
    }
}
