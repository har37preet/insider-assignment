package com.app.insider.data.model.OpenStreetMap;

public class AddressModel {
    String county;

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }
}
