package com.app.insider.data.model.home;

import java.util.ArrayList;

public class ShowModelObject {
    ArrayList<ShowModel> show;

    public ArrayList<ShowModel> getShow() {
        return show;
    }

    public void setShow(ArrayList<ShowModel> show) {
        this.show = show;
    }
}
