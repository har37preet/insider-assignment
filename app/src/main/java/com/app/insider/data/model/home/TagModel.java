package com.app.insider.data.model.home;

import com.google.gson.annotations.SerializedName;

public class TagModel {
    @SerializedName("is_featured")
    boolean isFeatured;
    @SerializedName("is_carousel")
    boolean isCarousel;
    @SerializedName("is_pick")
    boolean isPick;
    @SerializedName("is_primary_interest")
    boolean isPrimaryInterest;
    @SerializedName("_id")
    String id;
    @SerializedName("tag_id")
    String tagId;

    public boolean isFeatured() {
        return isFeatured;
    }

    public void setFeatured(boolean featured) {
        isFeatured = featured;
    }

    public boolean isCarousel() {
        return isCarousel;
    }

    public void setCarousel(boolean carousel) {
        isCarousel = carousel;
    }

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }

    public boolean isPrimaryInterest() {
        return isPrimaryInterest;
    }

    public void setPrimaryInterest(boolean primaryInterest) {
        isPrimaryInterest = primaryInterest;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }
}
