package com.app.insider.data.model.home;

import com.google.gson.annotations.SerializedName;

public class DisplayDetail {
    @SerializedName("link_type")
    String linkType;

    @SerializedName("link_slug")
    String linkSlug;

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getLinkSlug() {
        return linkSlug;
    }

    public void setLinkSlug(String linkSlug) {
        this.linkSlug = linkSlug;
    }
}
