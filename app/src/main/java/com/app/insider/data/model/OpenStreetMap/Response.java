package com.app.insider.data.model.OpenStreetMap;

public class Response {
    AddressModel address;

    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel address) {
        this.address = address;
    }
}
