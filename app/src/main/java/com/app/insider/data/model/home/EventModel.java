package com.app.insider.data.model.home;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EventModel {
    @SerializedName("_id")
    String id;
    @SerializedName("min_show_start_time")
    int minShowStartTime;
    String name;
    String type;
    String slug;
    @SerializedName("horizontal_cover_image")
    String horizontalCoverImage;
    @SerializedName("vertical_cover_image")
    String verticalCoverImage;
    ArrayList<TagModel> tags;
    String city;
    @SerializedName("venue_id")
    String venueId;
    @SerializedName("venue_name")
    String venueName;
    @SerializedName("venue_date_string")
    String venueDateString;
    @SerializedName("venue_geolocation")
    LocationModel venueGeoLocation;
    @SerializedName("is_rsvp")
    boolean isRSVP;
    @SerializedName("category_id")
    CategoryModel categoryModel;
    @SerializedName("group_id")
    GroupModel groupModel;
    @SerializedName("event_state")
    String eventState;
    @SerializedName("price_display_string")
    String priceDisplayString;
    @SerializedName("communication_strategy")
    String communicationStrategy;
    String model;
    @SerializedName("applicable_filters")
    ArrayList<String> applicableFilters;
    @SerializedName("popularity_score")
    float popularityScore;
    FavStats favStats;
    @SerializedName("purchase_visibility")
    String purchaseVisibility;
    @SerializedName("min_price")
    int minPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMinShowStartTime() {
        return minShowStartTime;
    }

    public void setMinShowStartTime(int minShowStartTime) {
        this.minShowStartTime = minShowStartTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getHorizontalCoverImage() {
        return horizontalCoverImage;
    }

    public void setHorizontalCoverImage(String horizontalCoverImage) {
        this.horizontalCoverImage = horizontalCoverImage;
    }

    public String getVerticalCoverImage() {
        return verticalCoverImage;
    }

    public void setVerticalCoverImage(String verticalCoverImage) {
        this.verticalCoverImage = verticalCoverImage;
    }

    public ArrayList<TagModel> getTags() {
        return tags;
    }

    public void setTags(ArrayList<TagModel> tags) {
        this.tags = tags;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueDateString() {
        return venueDateString;
    }

    public void setVenueDateString(String venueDateString) {
        this.venueDateString = venueDateString;
    }

    public LocationModel getVenueGeoLocation() {
        return venueGeoLocation;
    }

    public void setVenueGeoLocation(LocationModel venueGeoLocation) {
        this.venueGeoLocation = venueGeoLocation;
    }

    public boolean isRSVP() {
        return isRSVP;
    }

    public void setRSVP(boolean RSVP) {
        isRSVP = RSVP;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public GroupModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(GroupModel groupModel) {
        this.groupModel = groupModel;
    }

    public String getEventState() {
        return eventState;
    }

    public void setEventState(String eventState) {
        this.eventState = eventState;
    }

    public String getPriceDisplayString() {
        return priceDisplayString;
    }

    public void setPriceDisplayString(String priceDisplayString) {
        this.priceDisplayString = priceDisplayString;
    }

    public String getCommunicationStrategy() {
        return communicationStrategy;
    }

    public void setCommunicationStrategy(String communicationStrategy) {
        this.communicationStrategy = communicationStrategy;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public ArrayList<String> getApplicableFilters() {
        return applicableFilters;
    }

    public void setApplicableFilters(ArrayList<String> applicableFilters) {
        this.applicableFilters = applicableFilters;
    }

    public float getPopularityScore() {
        return popularityScore;
    }

    public void setPopularityScore(float popularityScore) {
        this.popularityScore = popularityScore;
    }

    public FavStats getFavStats() {
        return favStats;
    }

    public void setFavStats(FavStats favStats) {
        this.favStats = favStats;
    }

    public String getPurchaseVisibility() {
        return purchaseVisibility;
    }

    public void setPurchaseVisibility(String purchaseVisibility) {
        this.purchaseVisibility = purchaseVisibility;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }
}
