package com.app.insider.data.model.home;

import androidx.annotation.Nullable;

import java.util.Objects;

public class ShowModel {
    String display;
    String key;
    String type;
    boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        ShowModel showModelObject = (ShowModel) obj;
        return display.equals(((ShowModel) Objects.requireNonNull(obj)).getDisplay()) && key.equals(((ShowModel) Objects.requireNonNull(obj)).getKey());
    }
}
