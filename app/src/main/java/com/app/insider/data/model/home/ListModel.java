package com.app.insider.data.model.home;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class ListModel {
    LinkedHashMap<String, EventModel> masterList;
    Map<String, ArrayList<String>> groupwiseList;
    Map<String, ArrayList<String>> categorywiseList;

    public LinkedHashMap<String, EventModel> getMasterList() {
        return masterList;
    }

    public void setMasterList(LinkedHashMap<String, EventModel> masterList) {
        this.masterList = masterList;
    }

    public Map<String, ArrayList<String>> getGroupwiseList() {
        return groupwiseList;
    }

    public void setGroupwiseList(Map<String, ArrayList<String>> groupwiseList) {
        this.groupwiseList = groupwiseList;
    }

    public Map<String, ArrayList<String>> getCategorywiseList() {
        return categorywiseList;
    }

    public void setCategorywiseList(Map<String, ArrayList<String>> categorywiseList) {
        this.categorywiseList = categorywiseList;
    }
}
