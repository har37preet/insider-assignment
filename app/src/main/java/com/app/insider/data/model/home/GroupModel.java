package com.app.insider.data.model.home;

import com.google.gson.annotations.SerializedName;

public class GroupModel {
    @SerializedName("_id")
    String id;
    String name;
    String slug;
    @SerializedName("icon_img")
    String iconImg;

    public String getIconImg() {
        return iconImg;
    }

    public void setIconImg(String iconImg) {
        this.iconImg = iconImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
