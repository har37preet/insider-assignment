package com.app.insider.data;

import com.app.insider.data.local.ILocalDataSource;
import com.app.insider.data.local.LocalDataSource;
import com.app.insider.data.model.OpenStreetMap.Response;
import com.app.insider.data.model.home.HomeResponseModel;
import com.app.insider.data.remote.IRemoteDataSource;
import com.app.insider.data.remote.RetrofitServiceUtil;
import com.app.insider.utils.RxUtil;

import java.text.ParseException;
import java.util.HashMap;

import io.reactivex.Observable;

public class DataManager {
    private static DataManager mDataManager;
    private IRemoteDataSource mRemoteDataSource;
    private ILocalDataSource mLocalDataSource;

    private DataManager(IRemoteDataSource remoteDataSource, ILocalDataSource localDataSource) {
        mRemoteDataSource = remoteDataSource;
        mLocalDataSource = localDataSource;
    }

    public static DataManager getInstance() {
        if (mDataManager == null) {
            synchronized (DataManager.class) {
                if (mDataManager == null) {
                    mDataManager = new DataManager(RetrofitServiceUtil.create(), new LocalDataSource());
                }
            }
        }
        return mDataManager;
    }

    public Observable<HomeResponseModel> getHomeData(String city) {
        return mRemoteDataSource.getHomeData(1, "go-out", city).compose(RxUtil.applyScheduler())
                .map(RxUtil.unwrapResponse(null));
    }

    public Observable<HomeResponseModel> getResponse(String url) {
        return mRemoteDataSource.getResponse(url).compose(RxUtil.applyScheduler())
                .map(RxUtil.unwrapResponse(null));
    }

    public Observable<Response> getCity(String latitude, String longitude) {
        return mRemoteDataSource.getCity("https://nominatim.openstreetmap.org/reverse?format=json&lat="+ latitude + "&lon="+ longitude + "&addressdetails=1").compose(RxUtil.applyScheduler())
                .map(RxUtil.unwrapResponse(null));
    }
}
