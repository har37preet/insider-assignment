package com.app.insider.widget.loading;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;

import com.airbnb.lottie.LottieAnimationView;
import com.app.insider.R;

public class DialogProgressLoading extends AlertDialog {

    private LottieAnimationView mLogoImageView;

    private TextView mMsgTextView;

    private Context mContext;

    private String mMessage = "Loading...";

    private AnimationDrawable mAnimationDrawable;

    public DialogProgressLoading(@NonNull Context context) {
        this(context, R.style.dialog_progress_loading);
    }

    public DialogProgressLoading(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress_loading);
        mLogoImageView = (LottieAnimationView) findViewById(R.id.iv_logo);
        mMsgTextView = (TextView) findViewById(R.id.tv_msg);
        setCanceledOnTouchOutside(false);

        mLogoImageView.setAnimation("loader.json");
        mLogoImageView.playAnimation();
        mLogoImageView.loop(true);
    }

    @Override
    protected void onStart() {
        if (mMsgTextView != null) {
            mMsgTextView.setText(mMessage);
        }
        super.onStart();
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    @Override
    public void setCanceledOnTouchOutside(boolean cancel) {
        super.setCanceledOnTouchOutside(cancel);
    }

}