package com.app.insider.widget.loading;

public interface ILoading {
    void show();

    void hide();

    void dismiss();
}