package com.app.insider.utils.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.app.insider.BR;
import com.app.insider.R;
import com.app.insider.data.model.home.ShowModel;
import com.app.insider.ui.group.GroupViewModel;
import com.app.insider.utils.enums.FilterTypes;

import java.util.ArrayList;

public class FilterRVAdapter extends RecyclerView.Adapter<FilterRVAdapter.ViewHolder> {

    private Object mViewModel;
    private FilterTypes filterTypes;

    ArrayList<ShowModel> allFilters;

    public FilterRVAdapter(Object mViewModel, FilterTypes filterTypes) {
        this.mViewModel = mViewModel;
        this.filterTypes = filterTypes;
    }


    public void setAllFilters(ArrayList<ShowModel> allFilters) {
        this.allFilters = allFilters;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_filter, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ShowModel obj = allFilters.get(position);
        holder.bind(obj, position);
    }

    @Override
    public int getItemCount() {
        return allFilters==null?0:allFilters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setVariable(BR.filterType, filterTypes);
        }

        public void bind(Object obj, int index) {
            binding.setVariable(BR.viewModel, mViewModel);
            binding.setVariable(BR.showModel, obj);
            binding.executePendingBindings();
        }
    }
}
