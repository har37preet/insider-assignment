package com.app.insider.utils.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.app.insider.BR;
import com.app.insider.R;

import java.util.ArrayList;

public class CitiesRVAdapter extends RecyclerView.Adapter<CitiesRVAdapter.ViewHolder> {
    ArrayList<String> cities;

    private Object mViewModel;

    public CitiesRVAdapter(Object mViewModel) {
        this.mViewModel = mViewModel;
    }

    public void setCities(ArrayList<String> cities) {
        this.cities = cities;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_cities, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String city = cities.get(position);
        holder.bind(city, position);
    }

    @Override
    public int getItemCount() {
        return cities==null?0:cities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Object obj, int index) {
            binding.setVariable(BR.viewModel, mViewModel);
            binding.setVariable(BR.city, obj);
            binding.executePendingBindings();
        }
    }
}
