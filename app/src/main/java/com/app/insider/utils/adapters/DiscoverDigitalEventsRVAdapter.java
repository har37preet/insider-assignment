package com.app.insider.utils.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.app.insider.BR;
import com.app.insider.R;
import com.app.insider.data.model.home.DigitalEventGroupModel;
import com.app.insider.ui.home.HomeViewModel;

import java.util.ArrayList;

public class DiscoverDigitalEventsRVAdapter extends RecyclerView.Adapter<DiscoverDigitalEventsRVAdapter.ViewHolder> {
    private Object mViewModel;
    ArrayList<DigitalEventGroupModel> eventGroups;

    public DiscoverDigitalEventsRVAdapter(Object mViewModel) {
        this.mViewModel = mViewModel;
    }

    public void clearData(){
        this.eventGroups.clear();
        notifyDataSetChanged();
    }

    public void setEventGroups(ArrayList<DigitalEventGroupModel> eventGroups) {
        this.eventGroups = eventGroups;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_event_group, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DigitalEventGroupModel obj = eventGroups.get(position);
        holder.bind(obj, position);
    }

    @Override
    public int getItemCount() {
        return eventGroups==null?0:eventGroups.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Object obj, int index) {
            binding.setVariable(BR.eventGroupModel, obj);
            binding.setVariable(BR.viewModel, mViewModel);
            binding.executePendingBindings();
        }
    }
}
