package com.app.insider.utils.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.app.insider.BR;
import com.app.insider.R;
import com.app.insider.data.model.home.EventModel;

import java.util.ArrayList;

public class EventRVAdapter extends RecyclerView.Adapter<EventRVAdapter.EventViewHolder> {
    ArrayList<EventModel> events;
    private Object mViewModel;
    private boolean toLoadHorizontal= false;

    public EventRVAdapter(Object mViewModel) {
        this.mViewModel = mViewModel;
    }

    public void setEvents(ArrayList<EventModel> events) {
        this.events = events;
        for (int i = 0; i < events.size(); i++) {
            if (this.events.get(i).getVerticalCoverImage() == null) {
                toLoadHorizontal = true;
                break;
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_event, parent, false);
        return new EventViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        EventModel obj = events.get(position);
        holder.bind(obj, position);
    }

    @Override
    public int getItemCount() {
        return events==null?0:events.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public EventViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setVariable(BR.toLoadHorizontal,toLoadHorizontal);
        }

        public void bind(Object obj, int index) {
//            binding.setVariable(BR.title, title);
            binding.setVariable(BR.eventModel, obj);
            binding.executePendingBindings();
        }
    }
}
