package com.app.insider.utils.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.app.insider.BR;
import com.app.insider.R;
import com.app.insider.data.model.home.BannerModel;

import java.util.ArrayList;
import java.util.List;

public class BannerViewPagerAdapter extends RecyclerView.Adapter<BannerViewPagerAdapter.BannerViewHolder> {

    ArrayList<BannerModel> banners;
    private Object mViewModel;

    public BannerViewPagerAdapter(Object mViewModel) {
        this.mViewModel = mViewModel;
    }

    public void setBanners(ArrayList<BannerModel> banners) {
        this.banners = banners;
        notifyDataSetChanged();
    }

    public void clearData(){
        this.banners.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BannerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.activity_layout_banner, parent, false);
        return new BannerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BannerViewHolder holder, int position) {
        BannerModel obj = banners.get(position);
        holder.bind(obj, position);
    }

    @Override
    public int getItemCount() {
        return banners==null?0:banners.size();
    }

    public class BannerViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public BannerViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Object obj, int index) {
            binding.setVariable(BR.bannerProperty, obj);
            binding.setVariable(BR.viewModel, mViewModel);
//            binding.setVariable(BR.index, index);
            binding.executePendingBindings();
        }
    }
}
