package com.app.insider.utils;

import com.app.insider.data.constants.Constants;

import java.lang.reflect.Constructor;

import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class RxUtil {
    public static <T> ObservableTransformer<T, T> applyScheduler() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    }

    @SuppressWarnings("unchecked")
    public static <T> Function<T, T> unwrapResponse(Class<T> cls) {
        return baseResponse -> {
            return baseResponse;
        };
    }

}