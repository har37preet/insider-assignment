package com.app.insider.utils.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.insider.BR;
import com.app.insider.R;
import com.app.insider.data.model.home.BannerModel;
import com.app.insider.data.model.home.EventModel;
import com.app.insider.databinding.ActivityGroupCategoryListBinding;

import java.util.ArrayList;
import java.util.Map;

public class GroupsCategoriesRVAdapter extends RecyclerView.Adapter<GroupsCategoriesRVAdapter.ViewHolder>  {

    Map<String,ArrayList<EventModel>> groupsCategoriesList;
    private Object mViewModel;

    public GroupsCategoriesRVAdapter(Object mViewModel) {
        this.mViewModel = mViewModel;
    }

    public void clearData(){
        this.groupsCategoriesList.clear();
        notifyDataSetChanged();
    }

    public void setGroupsCategoriesList(Map<String, ArrayList<EventModel>> groupsCategoriesList) {
        if(this.groupsCategoriesList==null){
            this.groupsCategoriesList = groupsCategoriesList;
        }else{
            this.groupsCategoriesList.putAll(groupsCategoriesList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.activity_group_category_list, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String title = (String) groupsCategoriesList.keySet().toArray()[position];
        ArrayList<EventModel> obj = groupsCategoriesList.get(title);
        ActivityGroupCategoryListBinding itemBinding = (ActivityGroupCategoryListBinding) holder.binding;
        EventRVAdapter adapter = new EventRVAdapter(mViewModel);
        itemBinding.eventRecyclerView.setAdapter(adapter);
        adapter.setEvents(obj);
        holder.bind(title,obj, position);
    }

    @Override
    public int getItemCount() {
        return groupsCategoriesList==null?0:groupsCategoriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(String title, Object obj, int index) {
            binding.setVariable(BR.title, title);
            binding.setVariable(BR.list, obj);
            binding.setVariable(BR.viewModel, mViewModel);
            binding.executePendingBindings();
        }
    }
}
