package com.app.insider.ui.home;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;

import com.app.insider.R;
import com.app.insider.ui.base.BaseVMActivity;

public class HomeActivity extends BaseVMActivity {

    HomeViewModel viewModel;
    ViewBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new HomeViewModel(this);
        mBinding = binding(R.layout.activity_home, viewModel);
        viewModel.init(mBinding);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1202) {
            if(resultCode == RESULT_OK) {
                String city = data.getStringExtra("city");
                viewModel.changeCity(city);
            }
        }
    }
}
