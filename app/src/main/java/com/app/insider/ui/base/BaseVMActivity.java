package com.app.insider.ui.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.app.insider.BR;

public class BaseVMActivity extends BaseActivity {
    protected ViewDataBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected ViewDataBinding binding(int layoutId, BaseViewModel viewModel) {
        mBinding = DataBindingUtil.setContentView(this, layoutId);
        mBinding.setVariable(BR.viewModel, viewModel);
        return mBinding;
    }

    protected ViewDataBinding binding(int layoutId, BaseViewModel viewModel, Object store) {
        mBinding = DataBindingUtil.setContentView(this, layoutId);
        mBinding.setVariable(BR.viewModel, viewModel);
        return mBinding;
    }
}