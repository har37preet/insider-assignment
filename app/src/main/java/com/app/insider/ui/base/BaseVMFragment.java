package com.app.insider.ui.base;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.app.insider.BR;


public class BaseVMFragment extends BaseFragment {
    protected ViewDataBinding mBinding;

    protected ViewDataBinding binding(LayoutInflater inflater, ViewGroup container, int layoutId, BaseViewModel viewModel) {
        mBinding = DataBindingUtil.inflate(inflater, layoutId, container, false);
        mBinding.setVariable(BR.viewModel, viewModel);

        return mBinding;
    }
}