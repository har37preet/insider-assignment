package com.app.insider.ui.group;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;

import com.app.insider.R;
import com.app.insider.ui.base.BaseVMActivity;

public class GroupActivity extends BaseVMActivity {
    GroupViewModel viewModel;
    ViewBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new GroupViewModel(this);
        mBinding = binding(R.layout.activity_group, viewModel);
        String url = getIntent().getStringExtra("url");
        viewModel.init(mBinding, url);
    }
}
