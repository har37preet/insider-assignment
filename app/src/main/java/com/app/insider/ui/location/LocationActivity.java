package com.app.insider.ui.location;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;

import com.app.insider.R;
import com.app.insider.ui.base.BaseActivity;
import com.app.insider.ui.base.BaseVMActivity;

public class LocationActivity extends BaseVMActivity {
    LocationViewModel viewModel;
    ViewBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new LocationViewModel(this);
        mBinding = binding(R.layout.activity_location, viewModel);
        viewModel.init(mBinding);
    }
}
