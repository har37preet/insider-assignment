package com.app.insider.ui.group;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import com.app.insider.data.model.home.EventModel;
import com.app.insider.data.model.home.HomeResponseModel;
import com.app.insider.data.model.home.ShowModel;
import com.app.insider.data.model.home.ShowModelObject;
import com.app.insider.data.remote.NetworkObserver;
import com.app.insider.databinding.ActivityGroupBinding;
import com.app.insider.ui.base.BaseVMActivity;
import com.app.insider.ui.base.BaseViewModel;
import com.app.insider.utils.adapters.DiscoverDigitalEventsRVAdapter;
import com.app.insider.utils.adapters.EventRVAdapter;
import com.app.insider.utils.adapters.EventWideRVAdapter;
import com.app.insider.utils.adapters.FilterRVAdapter;
import com.app.insider.utils.enums.FilterTypes;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class GroupViewModel extends BaseViewModel {
    ActivityGroupBinding activityBinding;

    RecyclerView eventsRecyclerView;
    EventWideRVAdapter eventRVAdapter;

    private BottomSheetBehavior sheetBehavior;
    private LinearLayout bottom_sheet;

    public ObservableField<Boolean> show_time_filter = new ObservableField<>(true);
    public ObservableField<Boolean> show_sort_filter = new ObservableField<>(true);

    public ObservableField<String> selected_time_filter = new ObservableField<>("all");
    public ObservableField<String> selected_genre_filter = new ObservableField<>("all");

    public ObservableField<Boolean> show_shadow = new ObservableField<>(false);


    RecyclerView timeRecyclerView;
    FilterRVAdapter filterRVAdapter;

    ArrayList<EventModel> allEvents;
    ArrayList<EventModel> filteredEvents;

    ArrayList<ShowModel> allFilters;
    ArrayList<ShowModel> allGenres;


    RecyclerView genreRecyclerView;
    FilterRVAdapter genreFilterRVAdapter;



    public GroupViewModel(BaseVMActivity activity) {
        super(activity);
    }

    public void init(ViewBinding mBinding, String url) {
        activityBinding = (ActivityGroupBinding)mBinding;

        eventsRecyclerView = activityBinding.eventsRecyclerView;
        timeRecyclerView = activityBinding.timeRecyclerView;
        genreRecyclerView = activityBinding.genreRecyclerView;

        bottom_sheet = activityBinding.bottomSheet;
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState){
                    case BottomSheetBehavior.STATE_EXPANDED:
                        show_shadow.set(true);
                        break;
                    default:
                        show_shadow.set(false);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        eventsRecyclerView.setLayoutManager(mLayoutManager);
        eventRVAdapter = new EventWideRVAdapter(this);
        eventsRecyclerView.setAdapter(eventRVAdapter);

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        timeRecyclerView.setLayoutManager(mLayoutManager1);
        filterRVAdapter = new FilterRVAdapter(this, FilterTypes.TIME);
        timeRecyclerView.setAdapter(filterRVAdapter);

        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        genreRecyclerView.setLayoutManager(mLayoutManager2);
        genreFilterRVAdapter = new FilterRVAdapter(this, FilterTypes.GENRE);
        genreRecyclerView.setAdapter(genreFilterRVAdapter);

        getResponse(url);
    }

    public void onTouchOverlay(){
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
    private void getResponse(String url) {
        mDataManager.getResponse(url)
                .compose(mActivity.bindToLifecycle())
                .subscribe(new NetworkObserver<HomeResponseModel>(mActivity){
                    @Override
                    protected void onHandleSuccess(HomeResponseModel eventGroupResponseModel) {
                        super.onHandleSuccess(eventGroupResponseModel);
                        allEvents = new ArrayList<>();
                        allGenres = new ArrayList<>();
                        ShowModel filter1 = new ShowModel();
                        filter1.setDisplay("All");
                        filter1.setKey("all");
                        filter1.setSelected(true);
                        allGenres.add(filter1);
                        for (EventModel value : eventGroupResponseModel.getList().getMasterList().values()) {
                            allEvents.add(value);
                            ShowModel temp = new ShowModel();
                            temp.setDisplay(value.getCategoryModel().getName());
                            temp.setKey(value.getCategoryModel().getName());
                            if(!allGenres.contains(temp)) {
                                allGenres.add(temp);
                            }
                        }

                        genreFilterRVAdapter.setAllFilters(allGenres);

                        Map<String, ShowModelObject> filters= eventGroupResponseModel.getFilters();
                        allFilters = new ArrayList<>();
                        ShowModel filter2 = new ShowModel();
                        filter1.setDisplay("All");
                        filter1.setKey("all");
                        filter1.setSelected(true);
                        allFilters.add(filter1);
                        for (ShowModelObject value : eventGroupResponseModel.getFilters().values()) {
                            for(ShowModel showModel: value.getShow()){
                                if(!allFilters.contains(showModel)){
                                    allFilters.add(showModel);
                                }
                            }
                        }

                        filterRVAdapter.setAllFilters(allFilters);

                        Map<String, ArrayList<ShowModel>> sorters= eventGroupResponseModel.getSorters();
                        ArrayList<ShowModel> allSorters = new ArrayList<>();
                        for ( ArrayList<ShowModel> value : eventGroupResponseModel.getSorters().values()) {
                            for(ShowModel showModel: value){
                                if(!allSorters.contains(showModel)){
                                    allSorters.add(showModel);
                                }
                            }
                        }


                        if(allSorters==null||allSorters.isEmpty()){
                            filteredEvents = sortListByPrice(allEvents, true);
                            eventRVAdapter.setEvents(filteredEvents);
                        }
                        Log.d("","");

                    }

                    @Override
                    protected void onHandleError(String msg) {
                        super.onHandleError(msg);
                    }
                });
    }

    private ArrayList<EventModel> sortListByPrice(ArrayList<EventModel> allEvents, boolean isAsc){
        Collections.sort(allEvents,
                (o1, o2) -> o2.getMinPrice()<o1.getMinPrice()?(isAsc?1:-1):
                        (o2.getMinPrice() == o1.getMinPrice() ? 0 : (isAsc?-1:1)));
        return  allEvents;
    }

    public void filterFABClick(){
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
    }

    public void onTimeFilterSelected(ShowModel showModel, FilterTypes filterTypes){
        switch (filterTypes){
            case TIME:
                for(ShowModel filter : allFilters){
                    if(filter.getKey().equals(showModel.getKey())){
                        filter.setSelected(true);
                    }else{
                        filter.setSelected(false);
                    }
                }
                selected_time_filter.set(showModel.getKey());
                filterRVAdapter.notifyDataSetChanged();
                break;
            case GENRE:
                for(ShowModel filter : allGenres){
                    if(filter.getKey().equals(showModel.getKey())){
                        filter.setSelected(true);
                    }else{
                        filter.setSelected(false);
                    }
                }
                selected_genre_filter.set(showModel.getKey());
                genreFilterRVAdapter.notifyDataSetChanged();

                break;
            case SORT:
                break;
        }

        if(!selected_time_filter.get().equals("all")) {
            ArrayList<EventModel> temp = new ArrayList<>();
            for (EventModel eventModel : allEvents) {
                if (eventModel.getApplicableFilters().contains(selected_time_filter.get())) {
                    temp.add(eventModel);
                }
            }
            filteredEvents = temp;
        }else{
            filteredEvents = allEvents;
        }

        if(!selected_genre_filter.get().equals("all")) {
            ArrayList<EventModel> temp1 = new ArrayList<>();
            for (EventModel eventModel : filteredEvents) {
                if (eventModel.getCategoryModel().getName().equals(selected_genre_filter.get())) {
                    temp1.add(eventModel);
                }
            }
            filteredEvents = temp1;
        }

        eventRVAdapter.setEvents(filteredEvents);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

    }
}
