package com.app.insider.ui.home;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.databinding.ObservableField;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewpager2.widget.ViewPager2;

import com.app.insider.R;
import com.app.insider.data.model.home.DigitalEventGroupModel;
import com.app.insider.data.model.home.EventModel;
import com.app.insider.data.model.home.HomeResponseModel;
import com.app.insider.data.model.home.ShowModel;
import com.app.insider.data.remote.NetworkObserver;
import com.app.insider.databinding.ActivityHomeBinding;
import com.app.insider.ui.base.BaseVMActivity;
import com.app.insider.ui.base.BaseViewModel;
import com.app.insider.ui.group.GroupActivity;
import com.app.insider.ui.location.LocationActivity;
import com.app.insider.utils.adapters.BannerViewPagerAdapter;
import com.app.insider.utils.adapters.DiscoverDigitalEventsRVAdapter;
import com.app.insider.utils.adapters.GroupsCategoriesRVAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class HomeViewModel extends BaseViewModel {

    ActivityHomeBinding activityBinding;
    private ViewPager2 bannerViewPager;
    BannerViewPagerAdapter bannerViewPagerAdapter;

    RecyclerView groupsCategoriesRecyclerView;
    GroupsCategoriesRVAdapter groupsCategoriesRVAdapter;

    RecyclerView discoverDigitalEventsRecyclerView;
    DiscoverDigitalEventsRVAdapter discoverDigitalEventsRVAdapter;

    public ObservableField<String> discover_DE_Title = new ObservableField<>();
    public ObservableField<String> discover_DE_Desc = new ObservableField<>();

    public ObservableField<Boolean> discover_DE_Title_Visiblitiy = new ObservableField<>(true);
    public ObservableField<Boolean> discover_DE_Desc_Visiblitiy = new ObservableField<>(true);

    public HomeViewModel(BaseVMActivity activity) {
        super(activity);
    }

    public void init(ViewBinding mBinding) {
        activityBinding = (ActivityHomeBinding)mBinding;

        bannerViewPager = activityBinding.bannerViewPager;
        bannerViewPagerAdapter = new BannerViewPagerAdapter(this);
        bannerViewPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        transformBanner();
        autoSlideBanner();
        bannerViewPager.setAdapter(bannerViewPagerAdapter);

        groupsCategoriesRecyclerView = activityBinding.groupsCategoriesRecyclerView;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        groupsCategoriesRecyclerView.setLayoutManager(mLayoutManager);
        groupsCategoriesRVAdapter = new GroupsCategoriesRVAdapter(this);
        groupsCategoriesRecyclerView.setAdapter(groupsCategoriesRVAdapter);

        discoverDigitalEventsRecyclerView = activityBinding.discoverDigitalEventsRecyclerView;
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        discoverDigitalEventsRecyclerView.setLayoutManager(mLayoutManager1);
        discoverDigitalEventsRVAdapter = new DiscoverDigitalEventsRVAdapter(this);
        discoverDigitalEventsRecyclerView.setAdapter(discoverDigitalEventsRVAdapter);


        getHomeData("mumbai");

    }

    public void transformBanner() {
        bannerViewPager.setOffscreenPageLimit(1);
        float pageMargin = mActivity.getResources().getDimensionPixelOffset(R.dimen.viewpager_next_item_visible);
        float pageOffset = mActivity.getResources().getDimensionPixelOffset(R.dimen.viewpager_current_item_horizontal_margin);
        bannerViewPager.setPageTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float myOffset = position * -(2 * pageOffset + pageMargin);
                if (bannerViewPager.getOrientation() == ViewPager2.ORIENTATION_HORIZONTAL) {
                    if (ViewCompat.getLayoutDirection(bannerViewPager) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                        page.setTranslationX(-myOffset);
                    } else {
                        page.setTranslationX(myOffset);
                    }
                } else {
                    page.setTranslationY(myOffset);
                }
            }
        });
    }

    private void autoSlideBanner() {
        Handler handler = new Handler();
        Timer timer = new Timer();
        final Runnable runnable = new Runnable() {
            public void run() {
                int currentPage=bannerViewPager.getCurrentItem();
                bannerViewPager.setCurrentItem(++currentPage, true);
            }
        };
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        },3000,3000);
    }

    private void getHomeData(String city) {
        mDataManager.getHomeData(city)
                .compose(mActivity.bindToLifecycle())
                .subscribe(new NetworkObserver<HomeResponseModel>(mActivity){
                    @Override
                    protected void onHandleSuccess(HomeResponseModel homeResponseModel) {
                        super.onHandleSuccess(homeResponseModel);
                        Log.d("","");
                        bannerViewPagerAdapter.setBanners(homeResponseModel.getBanners());
                        Map<String, ArrayList<EventModel>> groupsCategoriesList = new LinkedHashMap<>();
                        if(homeResponseModel.getFeatured()!=null) groupsCategoriesList.put("Featured Events", homeResponseModel.getFeatured());
                        groupsCategoriesRVAdapter.setGroupsCategoriesList(groupsCategoriesList);
                        if(homeResponseModel.getPopular()!=null) groupsCategoriesList.put("Popular", homeResponseModel.getPopular());
                        groupsCategoriesRVAdapter.setGroupsCategoriesList(groupsCategoriesList);

                        if(homeResponseModel.getDigitalEventGroupModels()==null){
                            discover_DE_Title_Visiblitiy.set(false);
                            discover_DE_Desc_Visiblitiy.set(false);
                        }else{
                            discover_DE_Title.set("Discover Digital Events");
                            discover_DE_Desc.set(homeResponseModel.getDigitalEventGroupsDescription());
                            discoverDigitalEventsRVAdapter.setEventGroups(homeResponseModel.getDigitalEventGroupModels());
                        }

                        Map<String,EventModel> allEventsMap = homeResponseModel.getList().getMasterList();
                        ArrayList<EventModel> allEventsList = new ArrayList<>();
                        Map<String, Integer> genreCount = new HashMap<>();

                        for ( EventModel value : allEventsMap.values()) {
                            allEventsList.add(value);
                            genreCount.put(value.getCategoryModel().getName(), genreCount.get(value.getCategoryModel().getName())==null?1:genreCount.get(value.getCategoryModel().getName())+1);
                        }
                        Collections.sort(allEventsList,
                                (o1, o2) -> o2.getPopularityScore()<o1.getPopularityScore()?-1:
                                        (o2.getPopularityScore() == o1.getPopularityScore() ? 0 : 1));

                        genreCount = sortByComparator(genreCount, false);
                        Log.d("","");
                        for ( String genre : genreCount.keySet()) {
                            ArrayList<EventModel> events = new ArrayList<>();
                            for(EventModel event: allEventsList){
                                if(event.getCategoryModel().getName().equals(genre)) events.add(event);
                                if(events.size()==10) {
                                    groupsCategoriesList.put(genre, events);
                                    groupsCategoriesRVAdapter.setGroupsCategoriesList(groupsCategoriesList);
                                    break;
                                }
                            }
                        }

                    }

                    @Override
                    protected void onHandleError(String msg) {
                        super.onHandleError(msg);
                    }
                });
    }

    public void discoverDEClick(DigitalEventGroupModel digitalEventGroupModel){
        Intent intent = new Intent(mActivity, GroupActivity.class);
        intent.putExtra("url", digitalEventGroupModel.getUrl());
        mActivity.startActivity(intent);
    }

    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order)
    {

        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public void onLocationClick(){
        Intent intent = new Intent(mActivity, LocationActivity.class);
        mActivity.startActivityForResult(intent,1202);
    }

    public void changeCity(String city) {
        bannerViewPagerAdapter.clearData();
        groupsCategoriesRVAdapter.clearData();
        discoverDigitalEventsRVAdapter.clearData();
        discover_DE_Title_Visiblitiy.set(false);
        discover_DE_Desc_Visiblitiy.set(false);
        getHomeData(city.toLowerCase());
    }
}
