package com.app.insider.ui.base;

import android.content.Context;

import com.app.insider.data.DataManager;

public class BaseViewModel {
    protected BaseActivity mActivity;
    protected BaseFragment mFragment;
    protected DataManager mDataManager = DataManager.getInstance();

    public BaseViewModel(BaseVMActivity activity) {
        mActivity = activity;
    }

    public BaseViewModel(Context context, BaseFragment fragment) {
        mActivity = (BaseActivity) context;
        mFragment = fragment;
    }
}