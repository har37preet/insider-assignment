package com.app.insider.ui.location;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import com.app.insider.data.model.OpenStreetMap.Response;
import com.app.insider.data.remote.NetworkObserver;
import com.app.insider.databinding.ActivityLocationBinding;
import com.app.insider.ui.base.BaseVMActivity;
import com.app.insider.ui.base.BaseViewModel;
import com.app.insider.utils.adapters.CitiesRVAdapter;
import com.app.insider.utils.adapters.EventWideRVAdapter;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class LocationViewModel extends BaseViewModel {
    ActivityLocationBinding activityBinding;

    RecyclerView citiesRecyclerView;
    CitiesRVAdapter citiesRVAdapter;

    ArrayList<String> cities;

    private static final int REQUEST_LOCATION = 154;
    LocationManager locationManager;

    public LocationViewModel(BaseVMActivity activity) {
        super(activity);
    }

    public void init(ViewBinding mBinding) {
        activityBinding = (ActivityLocationBinding) mBinding;

        citiesRecyclerView = activityBinding.citiesRecyclerView;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        citiesRecyclerView.setLayoutManager(mLayoutManager);
        citiesRVAdapter = new CitiesRVAdapter(this);
        citiesRecyclerView.setAdapter(citiesRVAdapter);

        setCities();
    }

    private void setCities() {
        if(cities==null) cities = new ArrayList<>();
        cities.add("Bangalore");
        cities.add("Delhi");
        cities.add("Hyderabad");
        cities.add("Kolkata");
        cities.add("Mumbai");
        cities.add("Pune");

        citiesRVAdapter.setCities(cities);
    }

    public void locationSelectFromList(String city){
        Intent intent = new Intent();
        intent.putExtra("city", city);
        mActivity.setResult(RESULT_OK, intent);
        mActivity.finish();
    }

    public void detectLocation(){
        Dexter.withActivity(mActivity)
                        .withPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION
                ,Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){
                            getLocation();
                        }
                        if(report.getDeniedPermissionResponses()!=null && report.getDeniedPermissionResponses().size()>1 && report.getDeniedPermissionResponses().get(0).isPermanentlyDenied()){
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
                            intent.setData(uri);
                            mActivity.startActivity(intent);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
                        intent.setData(uri);
                        mActivity.startActivity(intent);
                    }
                }).check();
    }

    private void getLocation() {
        locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);

        Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (locationGPS != null) {
            double lat = locationGPS.getLatitude();
            double longi = locationGPS.getLongitude();
            String latitude = String.valueOf(lat);
            String longitude = String.valueOf(longi);
            getCity(latitude,longitude);
        } else {
            Toast.makeText(mActivity, "Unable to find location.", Toast.LENGTH_SHORT).show();
        }
    }

    private void getCity(String latitude, String longitude) {
        mDataManager.getCity(latitude,longitude)
                .compose(mActivity.bindToLifecycle())
                .subscribe(new NetworkObserver<Response>(mActivity){
                    @Override
                    protected void onHandleSuccess(Response response) {
                        super.onHandleSuccess(response);
                        String city = response.getAddress().getCounty();
                        locationSelectFromList(city);
                    }
                });
    }
}
