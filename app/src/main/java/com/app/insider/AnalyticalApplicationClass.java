package com.app.insider;

import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.facebook.stetho.Stetho;


public class AnalyticalApplicationClass extends MultiDexApplication {
    public static final String TAG = AnalyticalApplicationClass.class
            .getSimpleName();
    private static AnalyticalApplicationClass mInstance;
    public static synchronized AnalyticalApplicationClass getInstance() {
        return mInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Stetho.initializeWithDefaults(this);
    }

}
